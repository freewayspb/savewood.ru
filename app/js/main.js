jQuery(document).ready(function($) {

	/** CALLTOUCH */
	(function(w, d, e) {
		var a = 'all', b = 'tou'; var src = b + 'c' +'h'; src = 'm' + 'o' + 'd.c' + a + src;
		var jsHost = (("https:" == d.location.protocol) ? "https://" : "http://")+ src;
		s = d.createElement(e); p = d.getElementsByTagName(e)[0]; s.async = 1; s.src = jsHost +"."+"r"+"u/d_client.js?param;ref"+escape(d.referrer)+";url"+escape(d.URL)+";cook"+escape(d.cookie)+";";
		if(!w.jQuery) { jq = d.createElement(e); jq.src = jsHost  +"."+"r"+'u/js/jquery-1.5.1.min.js'; p.parentNode.insertBefore(jq, p);}
		p.parentNode.insertBefore(s, p);
	}(window, document, 'script'));

	/** LOADER */

	//$("body").queryLoader2({
	//    backgroundColor: "#b3d4fc url(../img/loader.gif) no-repeat 50%",
	//    barColor: "#00a651",
	//    barHeight: 0,
	//    percentage: true,
	//    onComplete : function() {
	//        $('body > section, body > ul, #help-nav').css('opacity', '1');
	//    }
	//
	//});

	/**
	 * @return {undefined}
	 */
	function displayWindowSize() {
		var bulk = $(window).height();
		var fn = $(window).width();
		if (bulk > fn) {
			bulk = fn;
		}
	}
	/**
	 * @param {string} direction
	 * @return {undefined}
	 */
	function animate(direction) {
		var next;
		var elem = slides.filter(".ProjectsSelected");
		next = elem[direction]();
		if (next.length) {
			elem.removeClass("ProjectsSelected");
			next.addClass("projects-slide ProjectsSelected");
		}
	}
	/**
	 * @return {undefined}
	 */
	//function updateNavPosition() {
	//    $(".swiper-news-nav .active-nav").removeClass("active-nav");
	//    var activeNav = $(".swiper-news-nav .swiper-slide").eq(contentSwiper.activeIndex).addClass("active-nav");
	//    if (!activeNav.hasClass("swiper-slide-visible")) {
	//        if (activeNav.index() > _this.activeIndex) {
	//            /** @type {number} */
	//            var thumbsPerNav = Math.floor(_this.width / activeNav.width()) - 1;
	//            _this.swipeTo(activeNav.index() - thumbsPerNav);
	//        } else {
	//            _this.swipeTo(activeNav.index());
	//        }
	//    }
	//}
	/**
	 * @return {undefined}
	 */
	function fn() {
		$(".swiper-technology-nav .active-nav").removeClass("active-nav");
		var activeNav = $(".swiper-technology-nav .swiper-slide").eq(pages.activeIndex).addClass("active-nav");
		if (!activeNav.hasClass("swiper-slide-visible")) {
			if (activeNav.index() > navSwiper.activeIndex) {
				/** @type {number} */
				var thumbsPerNav = Math.floor(navSwiper.width / activeNav.width()) - 1;
				navSwiper.swipeTo(activeNav.index() - thumbsPerNav);
			} else {
				navSwiper.swipeTo(activeNav.index());
			}
		}
	}
	/**
	 * @return {undefined}
	 */
	function init() {
		var result = document.getElementById('result'), // для отладки
			myPlacemark,
		// в этой версии координаты просто элементы массива (и они поменяны местами)
			destinations = {
				'Москва': [55.670748, 37.445305],
				'Москва2': [55.581551, 37.710018],
				'Санкт-Петербург': [59.90663, 30.26426],
				'Крымск': [44.934367, 37.985468],
				'Нальчик': [43.49418, 43.628209]
			},

		// Создание экземпляра карты и его привязка к контейнеру с
		// заданным id ("map").
			myMap = new ymaps.Map('map', {
				// При инициализации карты обязательно нужно указать
				// её центр и коэффициент масштабирования.
				center: destinations['Москва'], // Москва по умолчанию
				zoom: 16
			});
		myPlacemark = new ymaps.Placemark(destinations['Москва'], {
			hintContent: '\u0443\u043b. \u041e\u0437\u0435\u0440\u043d\u0430\u044f, \u0434\u043e\u043c 42, \u043e\u0444\u0438\u0441 203'
		}, {
			// Опции.
			// Необходимо указать данный тип макета.
			iconLayout: 'default#image',
			iconImageHref : "http://savewood.ru/img/map-icon.gif",
			iconImageSize: [54, 51],
			iconImageOffset: [-3, -42]
		});


		myMap.geoObjects.add(myPlacemark);

		// куда скакать
		function clickGoto() {

			// город
			var pos = $(this).data('coord'); // или this.getAttribute('title')
			var desc = $(this).find('.adress').html();

			// переходим по координатам
			myMap.panTo(destinations[pos], {
				flying: 1
			});
			myPlacemark = new ymaps.Placemark(destinations[pos], {
				hintContent: desc
			}, {
				// Опции.
				// Необходимо указать данный тип макета.
				iconLayout: 'default#image',
				iconImageHref : "http://savewood.ru/img/map-icon.gif",
				iconImageSize: [54, 51],
				iconImageOffset: [-3, -42]
			});
			myMap.geoObjects.add(myPlacemark);
			myMap.remove(myPlacemark).removeFromMap(myMap);
			return false;
		};

		// навешиваем обработчики
		var col = $('.js-map-link');
		for (var i = 0, n = col.length; i < n; ++i) {
			col[i].onclick = clickGoto;

		}
	}
	$(".promo-main-bg.layer").plaxify({
		"xRange" : 80,
		"yRange" : 80,
		"invert" : true
	});
	$(".cloud_2 span").plaxify({
		"xRange" : 40,
		"yRange" : 40
	});
	$(".cloud_4 span").plaxify({
		"xRange" : 70,
		"yRange" : 70
	});
	$.plax.enable();
	$("#kachestvo").parallax("50%");
	$(".layer-middle").parallax("140%", 0.8);
	$(".layer-front").parallax("100%", 0.3);
	$("a.hellow").click(function() {
		activate("#about");
	});
	$(".selectpicker").selectpicker({
		style : "select-input",
		size : 4
	});
	$(function() {
		$(window).resize(function() {
			var px = $(window).height();
			var qx = $(window).width();
			if (px > qx) {
				px = qx;
			}
			//$("#about").height(px + 67 + "px");
			var catalogPresentW = $(".catalog-projects .projects-slide > div").width();
			$(".catalog-projects .projects-slide > div").height(catalogPresentW * 75 / 100);
			var naviWidth = $(".navi-about span").width();
			$(".navi-about span").each(function(dataAndEvents) {
				if (dataAndEvents % 2 == 0) {
					$(this).height(naviWidth * 92 / 100);
				} else {
					$(this).height(naviWidth * 93 / 100);
				}
			});
		});
		var px = $(window).height();
		var qx = $(window).width();
		if (px > qx) {
			px = qx;
		}
		//$("#about").height(px + 67 + "px");
		var catalogPresentW = $(".catalog-projects .projects-slide > div").width();
		$(".catalog-projects .projects-slide > div").height(catalogPresentW * 75 / 100);
		$(window).load(function() {
			var naviWidth = $(".navi-about span").width();
			$(".navi-about span").each(function(dataAndEvents) {
				if (dataAndEvents % 2 == 0) {
					$(this).height(naviWidth * 92 / 100);
				} else {
					$(this).height(naviWidth * 93 / 100);
				}
			});
			return true;
		});
		/** @type {boolean} */
		var headerShowOnScroll = false;
		if ($("#navigation").hasClass("show-on-scroll")) {
			/** @type {boolean} */
			headerShowOnScroll = true;
		}
		$(window).scroll(function() {
			if ($(window).scrollTop() > 640 && headerShowOnScroll) {
				$("#navigation, #float_navigation").removeClass("show-on-scroll");
			} else {
				if ($(window).scrollTop() < 640 && (headerShowOnScroll && !$(".site-header").hasClass("show-mobile"))) {
					$("#navigation, #float_navigation").addClass("show-on-scroll");
				}
			}
			var currY = $(this).scrollTop();
			max = $(document).height();
			/** @type {string} */
			result = (currY / max * 100).toFixed(2);
			$("#navigation a.logo span").css("height", result + "%");
		});
	});
	var j = $("#projects .catalog-projects-wrapper").clone(true);
	$(document).on("click", ".filter-projects a", function() {
		var e = $(this).data("filter");
		j.find(".projects-slide:first-child").addClass("ProjectsSelected");
		var distanceToUserValue = j.html();
		$(".filter-projects a").removeClass("selected");
		$(this).addClass("selected");
		if (e !== "*") {
			var rule = j.clone(true);
			rule.find('.projects-slide > div[data-filter!="' + e + '"]').remove();
			$.each(rule.find(".projects-slide"), function() {
				if ($(this).find("div").length == 0) {
					$(this).remove();
				}
			});
			var items = rule.find(".projects-slide > div");
			items.unwrap();
			/** @type {number} */
			var itemIndex = 0;
			for (;itemIndex < items.length;itemIndex += 8) {
				items.slice(itemIndex, itemIndex + 8).wrapAll('<div class="projects-slide"></div>');
			}
			rule.find(".projects-slide:first-child").addClass("ProjectsSelected");
			distanceToUserValue = rule.html();
		}
		$("#projects .catalog-projects-wrapper").html(distanceToUserValue);
		window.slides = $(".catalog-projects-wrapper div");
		return false;
	});
	$(document).on("click", ".filters-catalog a", function() {
		var e = $(this).data("filter");
		$("#products").find("article.filters-catalog-decking:first-child").addClass("ProjectsSelected");
		$(".filters-catalog a").removeClass("selected");
		$(this).addClass("selected");
		if (e !== "*") {
			$("#products").find('article[class!="' + e + '"]').hide();
		} else {
			$("#products").find('article[data-catalog!="' + e + '"]').show();
		}
		$("#products").find('article[class="' + e + '"]').show();
		return false;
	});
	window.slides = $(".catalog-projects-wrapper div");
	$(document).on("click", ".catalog-projects-right", function() {
		animate("next");
		return false;
	});
	$(document).on("click", ".catalog-projects-left", function() {
		animate("prev");
		return false;
	});
	$("#full-products > a").click(function() {
		$(this).scrollTop(-67);
	});
	$(document).on("keyup", function(event) {
		var fmt = $("#float_navigation .current-menu-item").index();
		if (event.keyCode == 38) {
			if (fmt > 0) {
				/** @type {number} */
				fmt = fmt - 1;
				$("#float_navigation li:eq(" + fmt + ") a").click();
				console.log(fmt);
			}
			return false;
		}
		if (event.keyCode == 40) {
			if (fmt < 6) {
				fmt = fmt + 1;
				$("#float_navigation li:eq(" + fmt + ") a").click();
				console.log(fmt);
			}
			return false;
		}
	});
	$(document).on("click", ".nav-menu a", function(types) {
		types.preventDefault();
		activate($(this).attr("href"));
		$(".nav-menu li").removeClass("current-menu-item");
		$(this).parent().addClass("current-menu-item");
	});
	/** @type {string} */
	var username = "";
	$(".nav-menu a[href*=#]:not([href=#])").each(function(i) {
		if (username != "") {
			username += ", ";
		}
		username += $(".nav-menu a[href*=#]:not([href=#])").eq(i).attr("href");
	});
	$(username).waypoint(function(d) {
		if (d == "down") {
			$(".nav-menu a").parent().removeClass("current-menu-item");
			$('.nav-menu a[href="#' + $(this).attr("id") + '"]').parent().addClass("current-menu-item");
		}
	}, {
		offset : 0
	});
	$(username).waypoint(function(d) {
		if (d == "up") {
			$(".nav-menu a").parent().removeClass("current-menu-item");
			$('.nav-menu a[href="#' + $(this).attr("id") + '"]').parent().addClass("current-menu-item");
		}
	}, {
		/**
		 * @return {?}
		 */
		offset : function() {
			return-$(this).height() + 20;
		}
	});
	/**
	 * @param {string} element
	 * @return {undefined}
	 */
	var activate = function(element) {
		var targetOffset = $(element).offset().top;
		$("body, html").animate({
			scrollTop : targetOffset
		}, 500);
	};
	$(".sniper-projects-albom .catalog-sniper").click(function() {
		activate("#projects");
	});
	$(".sniper-projects-catalog .albom-sniper").click(function() {
		if ($(".sniper-projects-catalog .albom-sniper").hasClass("first-us")) {
			$(".catalog-projects .projects-slide:first-child > div:eq(0) a").click();
			$(this).removeClass("first-us");
		}
		activate("#catalog-projects-full");
		return false;
	});
	$(function() {
		var swiper = new Swiper(".swiper-container-about", {
			pagination : ".navi-about",
			createPagination : true,
			paginationClickable : true,
			keyboardControl : true,
			watchActiveIndex : true,
			speed : 500,
			//effect:'fade',
			nextButton : ".swiper-button-next",
			prevButton : ".swiper-button-prev",
			autoplayDisableOnInteraction : false,
			hashnav : true,
			/**
			 * @return {?}
			 */
			onSlideChangeStart : function() {
				return true;
			}
		});
	});
	var contentSwiper = new Swiper (".swiper-news-content",{
		keyboardControl : false,
		centeredSlides : true,
		nextButton : ".news-next",
		prevButton : ".news-prev"
		/**
		 * @return {undefined}
		 */
		//onSlideChangeStart : function() {
		//    //updateNavPosition();
		//}
	});


	//var _this = $(".swiper-news-nav").swiper({
	//    visibilityFullFit : true,
	//    slidesPerView : "auto",
	//    calculateHeight : true,
	//    offsetPxBefore : 430,
	//    /**
	//     * @return {undefined}
	//     */
	//    onSlideClick : function() {
	//        contentSwiper.swipeTo(_this.clickedSlideIndex);
	//    }
	//});
	var pages = $(".swiper-technology-content").swiper({
		keyboardControl : false,
		centeredSlides : true,
		/**
		 * @return {undefined}
		 */
		onSlideChangeStart : function() {
			fn();
		}
	});
	var navSwiper = $(".swiper-technology-nav").swiper({
		visibilityFullFit : true,
		slidesPerView : "auto",
		calculateHeight : true,
		/**
		 * @return {undefined}
		 */
		onSlideClick : function() {
			pages.swipeTo(navSwiper.clickedSlideIndex);
		}
	});
	$("#technology article > a.prev").click(function() {
		pages.swipePrev();
		fn();
		return false;
	});
	$("#technology article > a.next").click(function() {
		pages.swipeNext();
		fn();
		return false;
	});
	$("#news article > a.prev").click(function() {
		contentSwiper.swipePrev();
		//updateNavPosition();
		return false;
	});
	$("#news article > a.next").click(function() {
		contentSwiper.swipeNext();
		//updateNavPosition();
		return false;
	});
	$(".nav li a").click(function() {
		var matches = $(this).attr("href");
		if (matches == "#info") {
			/** @type {number} */
			matches = 0;
			$(".navi-about span:eq(" + matches + ")").click();
		}
		if (matches == "#news") {
			/** @type {number} */
			matches = 2;
			$(".navi-about span:eq(" + matches + ")").click();
		}
		if (matches == "#technology") {
			/** @type {number} */
			matches = 1;
			$(".navi-about span:eq(" + matches + ")").click();
		}
	});
	var swiper = new Swiper(".swiper-container-contacts", {
		grabCursor : true,
		direction : "vertical",
		slidesPerView : "auto",
		nextButton : ".contacts-next",
		prevButton : ".contacts-prev"
	});
	$(".contacts-prev").on("click", function(types) {
		types.preventDefault();
		swiper.swipePrev();
	});
	$(".contacts-next").on("click", function(types) {
		types.preventDefault();
		swiper.swipeNext();
	});
	$("#quality > article h2, #quality > article span, #quality > article img, #servise .service-list span").waypoint(function(dataAndEvents) {
		var $div = $(this);
		if (!$div.hasClass("animated")) {
			$div.addClass("animated");
		}
	}, {
		offset : "100%"
	});

//left navigation
	$(function() {
		var nextButton = $("#help-nav");
		$("#about").waypoint(function(i) {
			nextButton.toggleClass("not-horisont", i == "down");
		}, {
			offset : "-100%"
		});
		$("#about").waypoint(function(i) {
			nextButton.toggleClass("not-horisont", i == "up");
		}, {
			offset : "70%"
		});
		$("#promo").waypoint(function(i) {
			nextButton.toggleClass("top", i == "down");
		}, {
			offset : "-100%"
		});
		$("#promo").waypoint(function(i) {
			nextButton.toggleClass("top", i == "up");
		}, {
			offset : "70%"
		});
		$("#contacts").waypoint(function(index) {
			nextButton.toggleClass("bottom", index === "up");
		}, {
			/**
			 * @return {?}
			 */
			offset : function() {
				return $.waypoints("viewportHeight") - $(this).height() + 100;
			}
		});
	});
	$("#CatalogContainer a.close, #CatalogContainer #close-wrapper").click(function() {
		$("#full-products").hide();
		return false;
	});
	$('[data-form="clear"]').on("click", function() {
		$(".contact-form").trigger("reset");
		var set = $(this).parents("form.contact-form").find('input[type="text"], textarea');
		set.each(function(index) {
			set.eq(index).val("");
			set.eq(index).removeClass("error");
			set.eq(index).parent().find(".error").remove();
		});
		return false;
	});
	$(document).on("submit", "#contactForm", function() {
		$.ajax({
			url : $(this).prop("action"),
			type : "POST",
			dataType : "json",
			data : $("#contactForm").serialize(),
			/**
			 * @param {Object} response
			 * @param {?} textStatus
			 * @param {?} products
			 * @return {undefined}
			 */
			success : function(response, textStatus, products) {
				console.log(response);
				if (response.errors && $.isArray(response.errors)) {
					/** @type {string} */
					var label = "";
					$.each(response.errors, function(dataAndEvents, results) {
						/** @type {string} */
						label = label + "<span>" + results + "</span>";
					});
					$("#contactForm").find(".contact-info").addClass("errors").html(label);
				} else {
					if (response.success) {
						$("#contactForm").find(".contact-info").addClass("success").text(response.success);
					}
				}
			}
		});
		return false;
	});
	$(document).on("click", "#full-products .order", function() {
		$("#full-products").hide();
		activate("#contactForm");
		$("#contactForm input#name").focus();
		return false;
	});
	var target = $("#catalog-projects-full");
	target.cycle({
		"log" : "false",
		"fx" : "scrollHorz",
		"timeout" : 0,
		"slides" : "> article",
		"prev" : "#wrapper-catalog-projects-nav .photo-prev",
		"next" : "#wrapper-catalog-projects-nav .photo-next"
	});
	var $this = $("#wrapper-full-products");
	$this.cycle({
		"log" : "false",
		"fx" : "scrollHorz",
		"timeout" : 0,
		"speed" : 1,
		"slides" : "> article",
		"center-horz" : "true",
		"center-vert" : "true",
		"prev" : "#full-products .catalog-prev",
		"next" : "#full-products .catalog-next"
	});
	var slideshow = $("#slideshow").cycle({
		"slides" : "> article"
	});
	$(document).on("click", "#products > article, .project-albom .material a", function(types) {
		types.preventDefault();
		var errorClass = $(this).data("catalog");
		var cp = $("article#" + errorClass).index();
		$this.cycle("goto", cp - 3);
		$("#full-products").show();
	});
	$(document).on("click", ".catalog-projects .projects-slide a", function(types) {
		types.preventDefault();
		var name = $(this).data("project");
		var length = $("#catalog-projects-full article#project-" + name).index();
		target.cycle("goto", length - 2);
		$(".sniper-projects-catalog .albom-sniper").data("id", "#project-" + name);
		$("#catalog-projects-full").show();
		$(".sniper-projects-catalog .albom-sniper").removeClass("first-us");
		activate("#catalog-projects-full");
		$.waypoints("refresh");
	});
	ymaps.ready(init);
});
jQuery(document).ready(function($) {
	/**
	 * @return {undefined}
	 */
	function throttledUpdate() {
	}
	/**
	 * @param {Object} e
	 * @return {undefined}
	 */
	function handleMouseMove(e) {
		/** @type {number} */
		var slope1 = (-e.pageX + dist) / dist * bigNumber;
		/** @type {number} */
		var ew_mv_size = (e.pageY - int_mouse) / int_mouse * maxmv;
		if (slope1 > bigNumber) {
			/** @type {number} */
			slope1 = bigNumber;
		}
		if (slope1 < -bigNumber) {
			/** @type {number} */
			slope1 = -bigNumber;
		}
		if (ew_mv_size > maxmv) {
			/** @type {number} */
			ew_mv_size = maxmv;
		}
		if (ew_mv_size < -maxmv) {
			/** @type {number} */
			ew_mv_size = -maxmv;
		}
		$(".cd-floating-background").css({
			"-moz-transform" : "rotateX(" + ew_mv_size + "deg" + ") rotateY(" + slope1 + "deg" + ") translateZ(0)",
			"-webkit-transform" : "rotateX(" + ew_mv_size + "deg" + ") rotateY(" + slope1 + "deg" + ") translateZ(0)",
			"-ms-transform" : "rotateX(" + ew_mv_size + "deg" + ") rotateY(" + slope1 + "deg" + ") translateZ(0)",
			"-o-transform" : "rotateX(" + ew_mv_size + "deg" + ") rotateY(" + slope1 + "deg" + ") translateZ(0)",
			"transform" : "rotateX(" + ew_mv_size + "deg" + ") rotateY(" + slope1 + "deg" + ") translateZ(0)"
		});
	}
	/** @type {string} */
	var requestUrl = window.getComputedStyle(document.querySelector(".cd-background-wrapper"), "::before").getPropertyValue("content").replace(/"/g, "");
	/** @type {number} */
	var int_mouse = $(window).height() * 0.5;
	/** @type {number} */
	var dist = $(window).width() * 0.5;
	/** @type {number} */
	var bigNumber = 5;
	/** @type {number} */
	var maxmv = 3;
	var aspectRatio;
	$(".cd-floating-background").find("img").eq(0).load(function() {
		/** @type {number} */
		aspectRatio = $(this).width() / $(this).height();
		if (requestUrl == "web" && $("html").hasClass("preserve-3d")) {
			throttledUpdate();
		}
	}).each(function() {
		if (this.complete) {
			$(this).load();
		}
	});
	$(".promo__info-block").on("mousemove", function(e) {
		if (requestUrl == "web" && $("html").hasClass("preserve-3d")) {
			window.requestAnimationFrame(function() {
				handleMouseMove(e);
			});
		}
	});
	$(window).on("resize", function() {
		/** @type {string} */
		requestUrl = window.getComputedStyle(document.querySelector(".cd-background-wrapper"), "::before").getPropertyValue("content").replace(/"/g, "");
		if (requestUrl == "web" && $("html").hasClass("preserve-3d")) {
			window.requestAnimationFrame(function() {
				/** @type {number} */
				int_mouse = $(window).height() * 0.5;
				/** @type {number} */
				dist = $(window).width() * 0.5;
				throttledUpdate();
			});
		} else {
			$(".cd-background-wrapper").attr("style", "");
			$(".cd-floating-background").attr("style", "").removeClass("is-absolute");
		}
	});
});
(function getPerspective() {
	/** @type {Element} */
	var div = document.createElement("p");
	var elem = document.getElementsByTagName("html")[0];
	var field = document.getElementsByTagName("body")[0];
	var props = {
		"webkitTransformStyle" : "-webkit-transform-style",
		"MozTransformStyle" : "-moz-transform-style",
		"msTransformStyle" : "-ms-transform-style",
		"transformStyle" : "transform-style"
	};
	field.insertBefore(div, null);
	var name;
	for (name in props) {
		if (div.style[name] !== undefined) {
			/** @type {string} */
			div.style[name] = "preserve-3d";
		}
	}
	/** @type {(CSSStyleDeclaration|null)} */
	var compStyle = window.getComputedStyle(div, null);
	/** @type {string} */
	var preserve_3d = compStyle.getPropertyValue("-webkit-transform-style") || (compStyle.getPropertyValue("-moz-transform-style") || (compStyle.getPropertyValue("-ms-transform-style") || compStyle.getPropertyValue("transform-style")));
	if (preserve_3d !== "preserve-3d") {
		elem.className += " no-preserve-3d";
	} else {
		elem.className += " preserve-3d";
	}
	document.body.removeChild(div);

	/**
	 *
	 * FULL PRODUCTS slider
	 */
	$('.product__slider').each(function() {
		$(this).owlCarousel({
			navigation : true, // Show next and prev buttons
			slideSpeed : 300,
			paginationSpeed : 400,
			lazyLoad : true,
			singleItem:true,
			navigationText:	["<i class='glyphicon glyphicon-menu-left'></i>","<i class='glyphicon glyphicon-menu-right'></i>"]
		});
	});
	$('body').scrollspy({ target: '#navigation' });
})();